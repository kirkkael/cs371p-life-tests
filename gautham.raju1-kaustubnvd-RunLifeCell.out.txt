*** Life<Cell> 6x9 ***

Generation = 0, Population = 8.
---------
-----0---
--------0
0----00--
-------0-
0----0---

Generation = 4, Population = 19.
-------00
01----*--
----1-1--
01--.1--*
---0----*
00*-1-**-

*** Life<Cell> 6x8 ***

Generation = 0, Population = 11.
-0------
-0-00---
0-----0-
-0--0---
-0------
--0----0

*** Life<Cell> 6x8 ***

Generation = 0, Population = 9.
-0------
--------
0--00---
-0------
0--0--00
--------

*** Life<Cell> 6x5 ***

Generation = 0, Population = 12.
---0-
0-000
--0--
--00-
-0-0-
0-0--

Generation = 4, Population = 14.
-0-*1
---.1
1.-11
--1-0
--*.0
--0**

Generation = 8, Population = 12.
1-*..
-.*.*
*.-.*
*.**-
-1..-
*-1..

*** Life<Cell> 7x7 ***

Generation = 0, Population = 17.
--00-0-
--0----
--00---
-0---0-
000000-
------0
----0-0

*** Life<Cell> 8x7 ***

Generation = 0, Population = 12.
---00--
------0
0------
0------
-0-----
-0--0-0
---0---
-0----0

Generation = 1, Population = 25.
--01100
0--000-
10----0
1------
-10-0-0
010----
--0----
0-00-0-

Generation = 2, Population = 29.
001**11
1---1-0
*1000--
*00-0--
0------
1*100-0
-----0-
---1--0

Generation = 3, Population = 27.
---**--
-0-0-01
.*1-100
.1-010-
-1-0--0
-*--10-
000---0
--0-0--

Generation = 4, Population = 30.
-01**-1
1101---
***----
*-0-*-0
0--100-
-.--*10
11-0---
0-1--00

Generation = 5, Population = 26.
--*.*--
------1
...-1-0
*110*0-
11-*--0
-*-0.*1
-*0----
10--011

*** Life<Cell> 9x8 ***

Generation = 0, Population = 13.
--------
0---0---
-000----
--0-----
-----0--
0-0-----
-------0
----0-00
--------

Generation = 5, Population = 31.
-**.--0-
*-.0--0-
-.1*-**-
.0*-*--1
-.1-0--*
-0-0--*1
-1---*0*
---*.1..
0--0-*0.

*** Life<Cell> 9x7 ***

Generation = 0, Population = 13.
---0---
-------
--0-0--
--0--0-
0-0----
0-0--0-
-0-----
---0---
0------

*** Life<Cell> 10x8 ***

Generation = 0, Population = 8.
-------0
--------
------0-
-------0
0-------
-------0
--------
-------0
------0-
---0----

*** Life<Cell> 5x6 ***

Generation = 0, Population = 12.
-0----
--0--0
00-00-
0--000
0-----

*** Life<Cell> 10x8 ***

Generation = 0, Population = 16.
-0--00--
--------
--0----0
--0----0
0-------
00------
-0-0----
-----0--
---0--00
--------

Generation = 1, Population = 40.
0-00110-
-00-00-0
-010--01
0010--01
1-0----0
--00----
-1--00--
-0--0--0
--0-0-11
---0--00

Generation = 2, Population = 33.
-0--**1-
--1----1
-1-----*
-1--00-*
*0-0---1
0--1-0-0
0*---100
01-0---1
---01--*
-----0--

*** Life<Cell> 10x10 ***

Generation = 0, Population = 12.
----------
------00--
--------0-
0---------
0----0----
----------
-00---0---
----------
----0----0
-----0----

*** Life<Cell> 5x10 ***

Generation = 0, Population = 15.
00-----0--
---00-0---
----0---0-
-------000
00---0---0

Generation = 6, Population = 19.
***-.01-.1
1..*--*.-0
*---..10-.
01-**.-.1-
-..---0--.

*** Life<Cell> 6x10 ***

Generation = 0, Population = 13.
--0-0-----
-----0----
-------000
-00--0---0
--0----0--
---0------

Generation = 5, Population = 24.
------.10-
0*-*-*-*0*
--*--1.--1
.-0.1--*-*
.--*1--..*
-**-0--*-1

*** Life<Cell> 10x7 ***

Generation = 0, Population = 8.
-------
0--0---
0------
------0
-------
-0-----
-------
---00--
-------
----0--

*** Life<Cell> 10x10 ***

Generation = 0, Population = 8.
-----0----
----------
-------0--
---0------
----------
------0---
----------
--0-0-----
-0--------
-----0----

*** Life<Cell> 10x5 ***

Generation = 0, Population = 17.
-00-0
-----
----0
-00--
--0-0
-0---
00-00
0----
--0--
00---

*** Life<Cell> 9x7 ***

Generation = 0, Population = 11.
---0---
0------
---0---
----0-0
0------
--0----
-0----0
--0--0-
-------

*** Life<Cell> 8x10 ***

Generation = 0, Population = 10.
----------
0---------
-------0--
---------0
--00------
---00----0
----------
--0--0----

*** Life<Cell> 5x9 ***

Generation = 0, Population = 5.
-------0-
--0------
--0------
---------
------00-

*** Life<Cell> 8x10 ***

Generation = 0, Population = 15.
00-00-----
------0-00
-----0----
----------
------0---
---0------
----0----0
-0--0-0---

*** Life<Cell> 10x5 ***

Generation = 0, Population = 5.
-----
-0---
-----
---0-
-----
----0
---0-
-----
-0---
-----

*** Life<Cell> 9x5 ***

Generation = 0, Population = 13.
0---0
-00--
0-0--
-0-0-
0---0
-----
---0-
0--0-
-----

*** Life<Cell> 9x9 ***

Generation = 0, Population = 9.
-0-0--0--
---------
-----0---
---------
------0--
-----0---
------0--
---0-----
-------0-

Generation = 1, Population = 24.
0---00-0-
-0-0-00--
----0-0--
-----00--
-------0-
----0-0--
---0---0-
--0-0-00-
---0--0-0

Generation = 2, Population = 19.
----1-0-0
---------
-0-----0-
-----1---
----000-0
-------0-
----0-010
-0-------
-----010-

Generation = 3, Population = 39.
---0-----
-0--0-000
0-0--00-0
-0---*-00
---01110-
-----0010
-0-0--1*1
0-0-00--0
-0--01-10

Generation = 4, Population = 22.
-00---000
-----0---
---0-----
---0-.---
-001--*10
-0-------
--0--0-.*
---0---0-
---0---*-

*** Life<Cell> 8x7 ***

Generation = 0, Population = 11.
-0-----
-------
---0--0
-000---
0------
------0
---0---
-00----

Generation = 3, Population = 30.
01-0-0-
---*-0-
1001-*-
1.01*1*
10-0---
-0-*101
----0-0
-.*-10-

Generation = 6, Population = 21.
**0.-.-
---.0--
--.***0
-.--.*.
101---*
-*-..--
11----1
1..0*1-
